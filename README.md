# README #

* Implements Kruskal's minimum spanning tree algorithm.

### How do I get set up? ###

* Run gui2.java
* Either have the program auto-generate a graph for you, or use one of the .txt test files.

### Authors ###

* Christopher Helmer
* Jonathan Sobicinski
* Dylan Hansen

## Authors of additional code used ##

* Donald Chinn
* Ed Hong

### Who do I talk to? ###

* Christopher Helmer  
* christopher.helmer@yahoo.com