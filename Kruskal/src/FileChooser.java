
import java.awt.FlowLayout;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * 
 * @author Jonathan Sobicinski
 * @author Dylan Hansen
 *
 */
public class FileChooser {

	private String path;
	private RandomizeGraph rg;
	boolean loadFile = false;
	{

		Object[] options = { "LOAD GRAPH", "GENERATE GRAPH" };
		int option = JOptionPane.showOptionDialog(null,
		        "Would You Like To Load A Graph Or Generate A Graph?", "User Input Required",
		        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options,
		        options[0]);

		if (option == 0) {
			JFileChooser chooser = new JFileChooser(System.getProperty("user.home")
			        + "/Desktop");
			chooser.setDialogTitle("Please Select Graph Text File");
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Documents",
			        "txt");
			chooser.setFileFilter(filter);
			int returnVal = chooser.showOpenDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {

				path = chooser.getSelectedFile().getPath();
				loadFile = true;

			}
			if (returnVal == JFileChooser.CANCEL_OPTION) {
				System.exit(-1);
			}
		}

		else {
			SpinnerNumberModel nodeMode = new SpinnerNumberModel(3, 3, 500, 1);
			SpinnerNumberModel edgeMode = new SpinnerNumberModel(3, 3, 500, 1);
			SpinnerNumberModel weightMode = new SpinnerNumberModel(1, 1, 500, 1);

			JPanel panel = new JPanel(new FlowLayout());

			JLabel nodeLabel = new JLabel("Number Of Vertices: ");
			JLabel edgeLabel = new JLabel("Max Number Of Edges: ");
			JLabel weightLabel = new JLabel("Max Weight: ");

			JSpinner node = new JSpinner(nodeMode);
			JSpinner edge = new JSpinner(edgeMode);
			JSpinner weight = new JSpinner(weightMode);

			panel.add(nodeLabel);
			panel.add(node);
			panel.add(edgeLabel);
			panel.add(edge);
			panel.add(weightLabel);
			panel.add(weight);

			int optionTwo = JOptionPane.showOptionDialog(null, panel, "Enter valid number",
			        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, null,
			        null);

			if (optionTwo == 0) {
				rg = new RandomizeGraph((int) nodeMode.getValue(),
				        (int) weightMode.getValue(), (int) edgeMode.getValue());
			}
		}
	}

	public String getPath() {
		return path;
	}

	public RandomizeGraph getRandon() {	   
	    return rg;
    }
}
