
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * @author Jonathan Sobicinski
 *
 */
public class RandomizeGraph {

	int numberOfNodes;
	int maxWeight;
	int maxEdges;

	Random rand = new Random();
	StringBuilder sb = new StringBuilder();
	ArrayList<nodeConnection> nodeList = new ArrayList<nodeConnection>();

	public RandomizeGraph(int numberOfNodes, int maxWeight, int maxEdges) {

		this.numberOfNodes = numberOfNodes;
		this.maxWeight = maxWeight;
		this.maxEdges = maxEdges;

		if (maxEdges > numberOfNodes) {
			maxEdges = numberOfNodes;
		}

		for (int i = 0; i < numberOfNodes; i++) {

			int edges = rand.nextInt(maxEdges);
			ArrayList<Integer> al = getPossibleEdgeList(i);

			for (int j = 0; j < edges; j++) {
				int index = rand.nextInt(al.size());
				int weight = rand.nextInt(maxWeight) + 1;

				nodeConnection node = new nodeConnection(i, al.get(index), weight);
				nodeList.add(node);
				al.remove(index);
			}
		}

		ArrayList<nodeConnection> nodesToRemove = new ArrayList<nodeConnection>();

		for (int i = 0; i < nodeList.size(); i++) {

			for (int j = 0; j < nodeList.size(); j++) {

				nodeConnection nodeI = nodeList.get(i);
				nodeConnection nodeJ = nodeList.get(j);
				if (nodeI.Node == nodeJ.Edge && nodeI.Edge == nodeJ.Node) {
					nodesToRemove.add(nodeList.get(j));
				}
			}
		}

		nodeList.removeAll(nodesToRemove);

		for (int i = 0; i < nodeList.size(); i++) {
			sb.append(nodeList.get(i).Node + " " + nodeList.get(i).Edge + " "
			        + nodeList.get(i).Weight + "\n");
		}

	}

	private ArrayList<Integer> getPossibleEdgeList(int index) {

		ArrayList<Integer> al = new ArrayList<Integer>();
		for (int i = 0; i < numberOfNodes; i++) {

			al.add(i);
		}
		al.remove(index);

		return al;

	}

	public String getString() {
		return sb.toString();
	}

	private class nodeConnection {
		int Node;
		int Edge;
		int Weight;

		nodeConnection(int Node, int Edge, int Weight) {
			this.Node = Node;
			this.Edge = Edge;
			this.Weight = Weight;
		}

		/**
		 * @return the node
		 */
		public int getNode() {
			return Node;
		}

		/**
		 * @param node
		 *            the node to set
		 */
		public void setNode(int node) {
			Node = node;
		}

		/**
		 * @return the edge
		 */
		public int getEdge() {
			return Edge;
		}

		/**
		 * @param edge
		 *            the edge to set
		 */
		public void setEdge(int edge) {
			Edge = edge;
		}

	}

	public String outputText() {

		File file = new File("output.txt");
		try {
			PrintWriter writer = new PrintWriter(new FileOutputStream(file));

			String[] lines = sb.toString().split("\\n");
			for (String s : lines) {
				writer.write(s+"\n");
			
			}

			writer.close();
		}
		catch (FileNotFoundException e) {

			e.printStackTrace();
		}

		return file.getAbsolutePath();
	}
}
