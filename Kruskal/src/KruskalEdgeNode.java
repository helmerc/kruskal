
/**
 * Class that generates a KruskalEdgeNode and includes
 * methods to get the weight and get the original edge as
 * well as the start and end vertices. The data/weight is 
 * stored as a double so that we can implement comparable. 
 * @author Christopher Helmer
 */
public class KruskalEdgeNode implements Comparable {
	
	/** the first endpoint of the edge */
	private KruskalVertexNode v1;

	/** the second endpoint of the edge */
	private KruskalVertexNode v2;
	
	/**The edge that this node is derived from */
	private Edge originalEdge;
	
	/**The weight of the edge. */
	private double weight;

	/**
	 * Constructor for KruskalEdgeNode that sets the endpoints, casts the data
	 * from the input edge to a double so it can be used in the CompareTo
	 * method.
	 * @param e the input edge that will be transformed into a
	 * KruskalEdgeNode.
	 */
	public KruskalEdgeNode(Edge e) {
		this.v1 = new KruskalVertexNode(e.getFirstEndpoint());
		this.v2 = new KruskalVertexNode(e.getSecondEndpoint());
		this.weight = (Double) e.getData();
		this.originalEdge = e;
	
	}

	/**
	 * Return the first endpoint of this KruskalEdgeNode. ****Copied from
	 * Edge****
	 * 
	 * @return the first endpoint of this KruskalEdgeNode
	 */
	public KruskalVertexNode getFirstEndpoint() {
		return this.v1;
	}

	/**
	 * Return the second endpoint of this KruskalEdgeNode. ****Copied from
	 * Edge****
	 * 
	 * @return the second endpoint of this KruskalEdgeNode
	 */
	public KruskalVertexNode getSecondEndpoint() {
		return this.v2;
	}

	/**
	 * Return the weight associated with this KruskalEdgeNode. ****Copied
	 * somewhat from Edge****
	 * 
	 * @return the weight of this edge
	 */
	public Object getData() {
		return this.weight;
	}

	/**
	 * Get the edge that this KruskalEdgeNode refers to.
	 * 
	 * @return the edge that this KruskalEdgeNode refers to.
	 */
	public Edge getEdge() {
		return this.originalEdge;
	}

	/**
	 * Implements the compareTo method.
	 * 
	 * @param objthe other KruskalEdgeNode object.
	 * @return 0 if two objects are equal; less than zero if this object is
	 * smaller; greater than zero if this object is larger.
	 * @exception ClassCastException if obj is not a KruskalEdgeNode.
	 */
	public int compareTo(Object obj) {
		if (this.weight < ((KruskalEdgeNode) obj).weight) {
			return -1;
		}
		else if (this.weight == ((KruskalEdgeNode) obj).weight) {
			return 0;
		}
		else {
			return 1;
		}
	}	
}
