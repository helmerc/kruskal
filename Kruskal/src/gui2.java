


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;

/**
 * 
 * @author Jonathan Sobicinski
 * @author Dylan Hansen
 *
 */
public class gui2 extends JFrame {

	JPanel jp;
	Edge[] mst;
	GraphText gt;
	SimpleGraph G;
	final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	final int width;
	final int height;
	
	private String[] alphabet = new String[9999];
	ArrayList<String> listOfStrings;
	
	public gui2(SimpleGraph G, StringBuilder sb, ArrayList<String> listOfStrings)
	        throws EmptyHeapException {
		super("Kruskal's Algorithm");

		this.listOfStrings = listOfStrings;
		jp = new GPanel(listOfStrings);
		gt = new GraphText(mst, G, sb);
		this.setLayout(new BorderLayout());

		Kruskal k = new Kruskal(G);
		Edge[] mst = k.getMST();
		

		this.mst = mst;
		this.G = G;

		width = (int) (dim.getWidth()) - 100;
		height = (int) dim.getHeight() - 100;

		setSize(width, height);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		setLocation(dim.width / 2 - width / 2, dim.height / 2 - height / 2);

		add(jp, BorderLayout.CENTER);
		add(gt, BorderLayout.EAST);

	}
	

	class nodeRep {
		int x;
		int y;

		public nodeRep(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}
	}

	class GPanel extends JPanel {
		public GPanel(ArrayList<String> al) {

			setPreferredSize(new Dimension(300, 300));

		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);

			int sqrt = (int) Math.ceil(Math.sqrt(mst.length));
			int row = 0;
			int col = 0;

			ArrayList<nodeRep> al = new ArrayList<nodeRep>();

			for (int i = 0; i < G.numVertices(); i++) {
				

				int gtW = (gt.getWidth() / sqrt);
				int jpW = (jp.getWidth());
				int totalW = (jpW - gtW);

				int x = (totalW / sqrt * row + 50);
				int y = (height / sqrt * col + 50);
				g.drawOval(x, y, 20, 20);
				g.fillOval(x, y, 20, 20);

				nodeRep nr = new nodeRep(x, y);
				al.add(nr);

				row++;
				if (row == sqrt) {
					col++;
					row = 0;
				}

				g.drawString(listOfStrings.get(i), x, y - 10);

			}
			
			
			for (int j = 0; j < mst.length; j++) {
				

				int indexStart = 0;
				int indextEnd = 0;
				
				if (listOfStrings.contains(mst[j].getSecondEndpoint().getName().toString())) {
					for(int i = 0; i < listOfStrings.size(); i++) {
						if (listOfStrings.get(i) == mst[j].getFirstEndpoint().getName().toString()) {
							indexStart = i;
							break;
						} else {
							continue;
						}
					}
					
					for(int n = 0; n < listOfStrings.size(); n++) {
						if (listOfStrings.get(n) == mst[j].getSecondEndpoint().getName().toString()) {
							indextEnd = n;
							break;
						} else {
							continue;
						}
					}
					/*System.out.println("Draw to vertex "
					        + listOfStrings.get(indexStart) + " from "
					        + listOfStrings.get(indextEnd));*/
					g.drawLine(al.get(indexStart).getX() + 10,
					        al.get(indexStart).getY() + 10,
					        al.get(indextEnd).getX() + 10,
					        al.get(indextEnd).getY() + 10);

				}

			}

		}
		

	}

}