
import java.util.*;

/**
 * Class that generates a KruskalVertexNode and includes
 * methods to get the parent, set the parent, and get the name.
 * Name is stored as String so we can implement comparable.
 * @author Christopher Helmer
 *
 */
public class KruskalVertexNode {
	
	/**All the vertices this vertex is adjacent to */
	LinkedList incidentEdgeList;
	
	/**The name of this vertex as a String so it can be used in
	 * the ComparteTo method */
	private String name;
	
	/**The node that this node points to as its parent */
	private KruskalVertexNode parent;
	
	/**the x and y of this edge for graphical purposes */
	private int x;
	private int y;
	
	/**
	 * Constructor for the KruskalVertexNode class. Creates a
	 * KruskalVertexNode from a given Vertex and makes its parent
	 * itself.
	 * @param v The vertex to make the node from.
	 */
	public KruskalVertexNode(Vertex v) {
		this.incidentEdgeList = v.incidentEdgeList;
		this.name = (String) v.getName();
		this.parent = this;
		x = 0;
		y = 0;
	}
	
	/**
	 * Gets the parent of the current node
	 * @return the parent of the current node
	 */
	public KruskalVertexNode getParent() {
		return this.parent;
	}
	
	/**
	 * Set the parent of this vertex to the given node.
	 * @param k The KruskalVertexNode to set as this node's parent.
	 */
	public void setParent(KruskalVertexNode k) {
		this.parent = k;
	}
	
	/**
	 * Returns the name of the vertex.
	 * @return the name of the vertex.
	 */
	public String getName(){
		return this.name;
	}
	
	/**
     * Implements the compareTo method.
     * @param obj the other KruskalVertexNode object.
     * @return 0 if two objects are equal;
     *     less than zero if this object is smaller;
     *     greater than zero if this object is larger.
     * @exception ClassCastException if obj is not a KruskalVertexNode.
     */
    public int compareTo (Object obj) {
        if (this.name.compareTo(((KruskalVertexNode) obj).getName()) < 0) {
            return -1;
        } else if (this.name == (((KruskalVertexNode) obj).name))  {
            return 0;
        } else {
            return 1;
        }
    }

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}
    
 
}
