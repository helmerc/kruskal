
import java.util.*;
/**
 * This class takes a SimpleGraph as the input and runs kruskal's algorithm
 * on that graph to find the minimum spanning tree. There includes methods
 * for creating disjoint sets, putting edges into a heap, 
 * find(vertex), and union(root, root) as well
 * as a method to get an array of edges that comprise of 
 * the edges of a minimum spanning tree.
 * @author Christopher Helmer
 *
 */
public class Kruskal {
	
	/**hashtable of KruskalVertexNodes */
	private Hashtable vNodes;
	
	/**heap of KruskalEdgeNodes */
	private BinaryHeap heap;
	
	/**graph we are using for this class */
	private SimpleGraph graph;
	
	/**An array of edges that comprise of the minimum spanning tree of graph. */
	private Edge[] MSTedges;
	
	/**
	 * Constructor for Kruskal class
	 * @param g input graph
	 * @throws EmptyHeapException 
	 */
	public Kruskal(SimpleGraph g) throws EmptyHeapException {
		this.graph = g;
		this.vNodes = disjointSets();
		this.heap = edgeHeap();
		MSTedges = kruskalize();
	}
	
	/**
	 * Creates disjoint sets of size 1 such that each disjoint set
	 * is a single KruskalVertexNode whose root is itself.
	 * @param g the graph from which to derive the vertices for 
	 * the hash table.
	 * @return a hashtable that represents the individual disjoint sets
	 * for Kruskal's algorithm.
	 */
	public Hashtable disjointSets() {
		Hashtable KVNtable = new Hashtable();
		Iterator i;
		Vertex v;
		for (i= graph.vertices(); i.hasNext(); ) {
			v = (Vertex) i.next();
			KruskalVertexNode k = new KruskalVertexNode(v);
			KVNtable.put(k.getName(), k);
		}
		return KVNtable;
	}
	
	/**
	 * Build a heap of all the edges in the input graph.
	 * @param g the graph whose edges will be added to the heap.
	 * @return a heap of the edges of the graph.
	 */
	public BinaryHeap edgeHeap() {
		Iterator i;
		Edge e;
		int j;
		Comparable[] subarray = new KruskalEdgeNode[graph.numEdges()];
		for (i = graph.edges(), j = 0; i.hasNext(); j++) {
			e = (Edge) i.next();
			
			KruskalEdgeNode k = new KruskalEdgeNode(e);
			subarray[j] = k;
			
		}
		BinaryHeap h = BinaryHeap.buildHeap(subarray);
		return h;
	}
	
	/**
	 * This method does all of the steps of Kruskal's algorithm and returns
	 * an array of Edges that comprise of the minimum spanning tree.
	 * @return an array of Edges that comprise of the minimum spanning tree of the graph.
	 * @throws EmptyHeapException
	 */
	public Edge[] kruskalize() throws EmptyHeapException {
		int i = 0;
		KruskalEdgeNode k;
		KruskalVertexNode v, u;
		Edge[] mst = new Edge[graph.numVertices() - 1];
		while (i < graph.numVertices() - 1) {
			k = (KruskalEdgeNode) heap.deleteMin();
			v = find((KruskalVertexNode)vNodes.get(k.getFirstEndpoint().getName()));
			u = find((KruskalVertexNode)vNodes.get(k.getSecondEndpoint().getName()));
			if (u != v) {
				union(u, v);
				Edge e = k.getEdge();
				mst[i] = e;
				i++;
			}
		}
		return mst;
	}
	
	/**
	 * This method finds the root node of the input node 
	 * @param k is the input node that we are trying to find the root of
	 * @return the root node of the input node.
	 */
	public KruskalVertexNode find(KruskalVertexNode k) {
		
		if (k.getParent() == k) {
			return k;
		} else {
			
			return find(k.getParent());
		}
		
	}
	
	/**
	 * Unions two disjoint sets
	 * @param u is the root node of the first disjoint set
	 * @param v is the root node of the second disjoint set
	 */
	public void union(KruskalVertexNode u, KruskalVertexNode v) {
		if (u.getParent() == u && v.getParent() == v) {
			if (u.getParent().compareTo(v.getParent()) < 0 ){
				u.setParent(v);
			} else {
				v.setParent(u);
			}
		} else if (u.getParent() != u) {
			v.setParent(u);
		} else {
			u.setParent(v);
		}
		
		
	}
	
	/**
	 * Gets the array of edges of the minimum spanning tree
	 * @return the array of edges that comprise of the minimum spanning tree.
	 */
	public Edge[] getMST() {
		return this.MSTedges;
	}
}
