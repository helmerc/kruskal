
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * Class that runs the main method for the Kruskal assignment
 * @author Christopher Helmer
 * @author Jonathan Sobicinski
 * @author Dylan Hansen
 */
public class KruskalTest {

	public static String path;

	/**
	 * @param args
	 * @throws EmptyHeapException
	 */
	public static void main(String[] args) throws EmptyHeapException {

		FileChooser fc = new FileChooser();

		StringBuilder sb = new StringBuilder();

		RandomizeGraph rg;

		rg = fc.getRandon();

		if (fc.loadFile == true) {
			path = fc.getPath();
		}else
			path =rg.outputText();

		// Ask for input file and make a graph from that file
		sb.append(path);
		SimpleGraph G;
		G = new SimpleGraph();
		Hashtable vertTable = new Hashtable();
		vertTable = GraphInput.LoadSimpleGraph(G, path);
		ArrayList<String> al = new ArrayList<String>();

		// For each vertex, print out the name
		Vertex v;
		Edge e;
		Iterator i;
		sb.append("Iterating through vertices...\n");
		for (i = G.vertices(); i.hasNext();) {
			v = (Vertex) i.next();
			sb.append("found vertex " + v.getName() + "\n");
		}
		// For each vertex, print out the weight of each edge
		sb.append(("Iterating through adjacency lists...") + "\n");
		for (i = G.vertices(); i.hasNext();) {
			v = (Vertex) i.next();
			sb.append("Vertex " + v.getName() + "\n");
			al.add(v.getName().toString());
			Iterator j;

			for (j = G.incidentEdges(v); j.hasNext();) {
				e = (Edge) j.next();
				sb.append("  found edge " + e.getData() + "\n");
			}
		}
		// Print out all of the edges of the MST
		sb.append("\n");
		sb.append("The minimum spanning tree consists of the following edges:\n");
		// Run Kruskal's algorithm on the graph and put the edges into an array
		Kruskal k = new Kruskal(G);
		Edge[] mst = k.getMST();

		Double totalMST = 0.0;
		// Print out the edges as vertex pairs of the MST and the weight of each
		// edge and add them all up
		for (int j = 0; j < mst.length; j++) {
			sb.append(mst[j].getFirstEndpoint().getName() + ", "
			        + mst[j].getSecondEndpoint().getName() + ", weight: " + mst[j].getData()
			        + "\n");
			totalMST += (Double) mst[j].getData();

		}

		// print out the total weight of all the edges.
		sb.append("Total weight for the Minimum Spanning Tree: " + totalMST + "\n");

		gui2 g2 = new gui2(G, sb, al);
		g2.setVisible(true);
	}
}
