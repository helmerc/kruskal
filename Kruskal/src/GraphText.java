
import java.awt.BorderLayout;
import java.util.Iterator;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * 
 * @author Jonathan Sobicinski
 * @author Dylan Hansen
 *
 */
public class GraphText extends JPanel {

	/**
	 * Create the panel.
	 * 
	 * @param mst
	 * @param g
	 */
	public GraphText(Edge[] mst, SimpleGraph G, StringBuilder sb) {
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		JTextArea message = new JTextArea();
		message.setText(sb.toString());

		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(message);

	}

}
